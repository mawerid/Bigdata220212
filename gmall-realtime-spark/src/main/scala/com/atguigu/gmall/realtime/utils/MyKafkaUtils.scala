package com.atguigu.gmall.realtime.utils

import java.util

import org.apache.kafka.clients.consumer
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.TopicPartition
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}

import scala.collection.mutable

/**
  * Kafka工具类, 用于从Kafka中消费数据 及 往Kakfa生产数据
  */
object MyKafkaUtils {


  /**
    * 消费者配置
    *
    * 消费者配置类: ConsumerConfig
    */
  private val consumerParams: mutable.Map[String, Object] = mutable.Map[String,Object](
    //Kafka集群位置
    //ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> "hadoop102:9092,hadoop103:9092,hadoop104:9092" ,
    ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> MyPropsUtils(MyConfig.KAFKA_SERVERS) ,

    //key和value的反序列化器
    ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> "org.apache.kafka.common.serialization.StringDeserializer" ,
    ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> "org.apache.kafka.common.serialization.StringDeserializer",
    //消费者组
    //ConsumerConfig.GROUP_ID_CONFIG -> "aaa"
    //offset重置  earliest | latest
    ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "latest",
    //offset提交 自动 | 手动
    ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG -> "true",
    //自动提交间隔
    ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG -> "5000"

  )

  /**
    * 从kafka中消费数据 , 基于默认的offset（Kafka说了算）
    * 基于SparkStreaming(StreamingContext)对接Kafka ，获取到DStream
    */
  def getKakfaDStream(ssc: StreamingContext , topic : String  , groupId: String ) ={
    //设置消费者组
    consumerParams.put(ConsumerConfig.GROUP_ID_CONFIG , groupId)

    val kafkaDStream: InputDStream[ConsumerRecord[String, String]] = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](Array(topic), consumerParams))
    kafkaDStream
  }

  /**
    * 从kafka中消费数据 , 基于指定的offset(咱们说了算)
    * 基于SparkStreaming(StreamingContext)对接Kafka ，获取到DStream
    */
  def getKakfaDStream(ssc: StreamingContext , topic : String  , groupId: String , offsets: Map[TopicPartition,Long] ) ={
    //设置消费者组
    consumerParams.put(ConsumerConfig.GROUP_ID_CONFIG , groupId)

    val kafkaDStream: InputDStream[ConsumerRecord[String, String]] = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](Array(topic), consumerParams,offsets))
    kafkaDStream
  }

  /**
    * 创建生产者对象
    *
    * 生产者配置对象: ProducerConfig
    */
  def createProducer(): KafkaProducer[String, String] = {
    val producerParams: util.HashMap[String, AnyRef] = new util.HashMap[String,AnyRef]()
    //kafka集群位置
    //producerParams.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG , "hadoop102:9092,hadoop103:9092,hadoop104:9092")
    //producerParams.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG , MyPropsUtils("kafka.servers"))
    producerParams.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG , MyPropsUtils(MyConfig.KAFKA_SERVERS))

    //key和value的序列化器
    producerParams.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG , "org.apache.kafka.common.serialization.StringSerializer")
    producerParams.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    //acks  retries  batch.size  linger.ms


    val producer: KafkaProducer[String, String] = new KafkaProducer[String,String](producerParams)
    producer
  }

  /**
    * 生产者对象
    */
  private val producer : KafkaProducer[String,String] = createProducer()


  /**
    * 往Kafka生产数据
    */
  def send(topic: String , msg : String ): Unit ={
    producer.send(new ProducerRecord[String,String](topic,msg))
  }


  /**
    * 强制将缓冲区的数据刷出
    */
  def flush(): Unit ={
    producer.flush()
  }

}
