package com.atguigu.gmall.realtime.app

import java.text.SimpleDateFormat
import java.time.{LocalDate, Period}
import java.{lang, util}
import java.util.Date

import com.alibaba.fastjson.{JSON, JSONObject}
import com.atguigu.gmall.realtime.bean.{DauInfo, PageLog}
import com.atguigu.gmall.realtime.utils.{MyBeanUtils, MyEsUtils, MyKafkaUtils, MyOffsetUtils, MyRedisUtils}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.TopicPartition
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{HasOffsetRanges, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import redis.clients.jedis.{Jedis, Pipeline}

import scala.collection.mutable.ListBuffer

/**
  * 日活宽表任务
  *
  * 1. 准备实时环境
  * 2. 从Redis中读取offset
  * 3. 从Kafka中消费数据
  * 4. 提取Offset
  * 5. 处理数据
  *    5.1 转换结构
  *    5.2 去重
  *    5.3 维度关联
  * 6. 写出到ElasticSearch
  * 7. 提交Offset
  *
  */
object DwdDauApp {



  def main(args: Array[String]): Unit = {
    //TODO 补充 程序启动时，进行状态还原
    revertState()

    //1. 准备实时环境
    val sparkConf: SparkConf = new SparkConf().setAppName("dwd_dau_app").setMaster("local[4]")
    val ssc: StreamingContext = new StreamingContext(sparkConf, Seconds(5))

    //2. 从Redis中读取offset
    val topic : String = "DWD_PAGE_TOPIC_0212"
    val groupId : String = "DWD_DAU_GROUP"
    val offsets: Map[TopicPartition, Long] = MyOffsetUtils.readOffset(topic, groupId)

    //3. 从kafka中消费数据
    var kafkaDStream: InputDStream[ConsumerRecord[String, String]] = null
    if(offsets.size > 0 ){
      kafkaDStream = MyKafkaUtils.getKakfaDStream(ssc, topic , groupId , offsets)
    }else{
      kafkaDStream = MyKafkaUtils.getKakfaDStream(ssc, topic , groupId )
    }

    //4. 提取offset
    var  offsetRanges: Array[OffsetRange] = null
    val offsetDStream: DStream[ConsumerRecord[String, String]] = kafkaDStream.transform(
      rdd => {
        offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
        rdd
      }
    )

    //5. 处理数据
    //5.1 转换结构
    val pageLogDStream: DStream[PageLog] = offsetDStream.map(
      consumerRecord => {
        val msg: String = consumerRecord.value()
        //转换成PageLog对象
        val pageLog: PageLog = JSON.parseObject(msg, classOf[PageLog])
        pageLog
      }
    )
    //pageLogDStream.print(100)

    //5.2 去重
    //自我审查: 将页面数据中last_page_id不为空的数据过滤掉.
    pageLogDStream.cache()
    pageLogDStream.foreachRDD(
      rdd => {
        println("自我审查前: " + rdd.count())
      }
    )

    val filterDStream: DStream[PageLog] = pageLogDStream.filter(
      pageLog => {
        pageLog.last_page_id == null
      }
    )

    filterDStream.cache()
    filterDStream.foreachRDD(
      rdd => {
        println("自我审查后: " + rdd.count())
        println("---------------------------")
      }
    )

    //第三方审查: 在Redis中维护今日访问过的mid, 通过自我审查后的结果再到Redis中与维护的mid进行比对，如果已经存在当前页面数据对应的mid,
    //           当前数据就直接扔掉. 反之， 将当前页面数据对应的mid记录到redis中，并将当前页面数据保留下来，最终写入到ES中。
    // Redis中如何存储:
    // type:        set
    // key:      DAU:MID:[date]
    // value:    mid的集合
    // 写入API:   sadd
    // 读取API:   smembers
    // 是否过期: 24小时过期


    //filter方法一般可以通过mapPartition进行优化
    val redisFilterDStream: DStream[PageLog] = filterDStream.mapPartitions(
      pageLogIter => {
        val list: List[PageLog] = pageLogIter.toList
        println("第三方审查前: " + list.size)
        val pageLogs: ListBuffer[PageLog] = ListBuffer[PageLog]()
        val jedis: Jedis = MyRedisUtils.get()
        val sdf: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
        for (pageLog <- list) {
          //从pageLog中提取mid
          val mid: String = pageLog.mid
          val ts: Long = pageLog.ts
          val dateStr: String = sdf.format(new Date(ts))

          //判断redis中维护的mid中是否包含当前数据的mid
          /*
          //假定我们在redis中使用list维护mid
          val redisListKey : String = s"DAU:MID:$dateStr"
          val mids: util.List[String] = jedis.lrange(redisListKey ,0 , -1)

          //1. 判断是否存在
          //2. 基于第一步的结果决定是否要将当前mid记录到redis中 并  保留当前数据.
          if(!mids.contains(mid)){
            //把当前的mid记录到redis中
            jedis.lpush(redisListKey , mid)
            //当前页面数据保留下来
            pageLogs.append(pageLog)
          }
          */

          //假定我们在redis中使用Set维护mid
          /*
          val redisSetKey : String = s"DAU:MID:$dateStr"
          val mids: util.Set[String] = jedis.smembers(redisSetKey)
          if(!mids.contains(mid)){
            jedis.sadd(redisSetKey , mid )
            pageLogs.append(pageLog)
          }
          */
          val redisSetKey: String = s"DAU:MID:$dateStr"
          val exists: lang.Long = jedis.sadd(redisSetKey, mid)
          if (exists == 1L) {
            pageLogs.append(pageLog)
          }

        }
        MyRedisUtils.close(jedis)
        println("第三方审查后: " + pageLogs.size)
        pageLogs.toIterator
      }
    )
    //redisFilterDStream.print(100)

    //5.3 维度关联
    //  pageLog + 维度数据 + 日期字段 => DauInfo
    val dauInfoDStream: DStream[DauInfo] = redisFilterDStream.mapPartitions(
      pageLogIter => {
        val list: List[PageLog] = pageLogIter.toList
        val dauinfos: ListBuffer[DauInfo] = ListBuffer[DauInfo]()
        val jedis: Jedis = MyRedisUtils.get()
        val sdf: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        for (pageLog <- list) {
          val dauInfo: DauInfo = new DauInfo()
          //pageLog中已有的字段拷贝到DauInfo中
          MyBeanUtils.copyFieldValueByMethod(pageLog , dauInfo)

          //关联用户维度信息
          val userId: String = pageLog.user_id
          var redisUserKey : String = s"DIM:USER_INFO:$userId"
          val userJson: String = jedis.get(redisUserKey)
          val userJsonObj: JSONObject = JSON.parseObject(userJson)
          //提取用户性别
          val userGender: String = userJsonObj.getString("gender")
          //提取用户生日
          val userBirthday: String = userJsonObj.getString("birthday")
          //换算年龄
          val userBirthdayLd: LocalDate = LocalDate.parse(userBirthday)
          val nowLd: LocalDate = LocalDate.now()
          val period: Period = Period.between(userBirthdayLd, nowLd)
          val userAge: Int = period.getYears
          //补充到DauInfo中
          dauInfo.user_gender = userGender
          dauInfo.user_age = userAge.toString

          //关联地区维度信息
          val provinceId: String = pageLog.province_id
          val redisProvinceKey : String = s"DIM:BASE_PROVINCE:$provinceId"
          val provinceJson: String = jedis.get(redisProvinceKey)
          val provinceJsonObj: JSONObject = JSON.parseObject(provinceJson)
          //补充到dauInfo中
          dauInfo.province_name =  provinceJsonObj.getString("name")
          dauInfo.province_iso_code = provinceJsonObj.getString("iso_code")
          dauInfo.province_3166_2 = provinceJsonObj.getString("iso_3166_2")
          dauInfo.province_area_code = provinceJsonObj.getString("area_code")

          //处理日期字段
          val dtHr: String = sdf.format(new Date(pageLog.ts))
          val dtHrArr: Array[String] = dtHr.split(" ")
          dauInfo.dt = dtHrArr(0)
          dauInfo.hr = dtHrArr(1).split(":")(0)

          dauinfos.append(dauInfo)
        }
        MyRedisUtils.close(jedis)

        dauinfos.toIterator
      }
    )
    //dauInfoDStream.print(100)

    //6. 将处理好的数据写入到ES对应的index中.
    //1. 索引规划:
    // index: gmall_dau_info_date
    // 索引分割 ：一天一个索引
    // 索引别名 ：按照需求指定别名
    // 索引模板 :  "index_patterns": ["gmall_dau_info*"]

    //2. 写入方式:  批量幂等写入

    dauInfoDStream.foreachRDD(
      rdd => {
        rdd.foreachPartition(
          dauInfoIter => {
            val docs: List[(String, DauInfo)] = dauInfoIter.map( dauInfo => ( dauInfo.mid , dauInfo) ).toList
            if(docs.size > 0 ){
              val dauInfo: DauInfo = docs.head._2
              val date: String = dauInfo.dt
              val indexName : String = s"gmall_dau_info_$date"
              MyEsUtils.bulkSave(indexName , docs)
            }
          }
        )
      // 提交Offset
      MyOffsetUtils.saveOffset(topic , groupId , offsetRanges)
      }
    )
    ssc.start()
    ssc.awaitTermination()
  }

  /**
    * 状态还原
    * 程序启动时，进行状态还原, 将ES中所记录的数据中的mid提取处理，覆盖到Redis中（将Redis中记录的mid都删除，将ES中的mid写入到Redis）
    *
    * @return
    */
  def revertState() = {
    //直接清空Redis中的mid
    val date: LocalDate = LocalDate.now()
    val jedis: Jedis = MyRedisUtils.get()
    val redisDauKey : String = s"DAU:MID:${date.toString}"
    jedis.del(redisDauKey)

    //从ES中查询所有的mid
    val indexName : String = s"gmall_dau_info_${date.toString}"
    val fieldName : String ="mid"
    val mids: List[String] = MyEsUtils.searchField(indexName , fieldName)
    if(mids != null && mids.size > 0 ){
      val pipeline: Pipeline = jedis.pipelined()
      for (mid <- mids) {
        // jedis.sadd(redisDauKey , mid )
        pipeline.sadd(redisDauKey , mid ) //不会发送到redis
      }
      pipeline.sync()  //真正的到redis执行
    }
  }
}







