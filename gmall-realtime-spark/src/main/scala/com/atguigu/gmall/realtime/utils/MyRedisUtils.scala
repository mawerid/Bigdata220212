package com.atguigu.gmall.realtime.utils

import redis.clients.jedis.{Jedis, JedisPool, JedisPoolConfig}

/**
  * Redis工具类,用于获取Jedis对象
  */
object MyRedisUtils {

  def main(args: Array[String]): Unit = {
    val jedis: Jedis = get()
    println(jedis)
    close(jedis)

  }


  def close(jedis: Jedis ): Unit ={
    if(jedis != null ) jedis.close()
  }

  def get(): Jedis={
    jedisPool.getResource
  }

  def createJedisPool(): JedisPool = {
    val redisHost : String = MyPropsUtils(MyConfig.REDIS_HOST)
    val redisPort : Int = MyPropsUtils(MyConfig.REDIS_PORT).toInt

    val jedisPoolConfig = new JedisPoolConfig()
    jedisPoolConfig.setMaxTotal(100) //最大连接数
    jedisPoolConfig.setMaxIdle(20) //最大空闲
    jedisPoolConfig.setMinIdle(20) //最小空闲
    jedisPoolConfig.setBlockWhenExhausted(true) //忙碌时是否等待
    jedisPoolConfig.setMaxWaitMillis(5000) //忙碌时等待时长 毫秒
    jedisPoolConfig.setTestOnBorrow(true) //每次获得连接的进行测试

    val jedisPool: JedisPool = new JedisPool(jedisPoolConfig, redisHost ,redisPort)
    jedisPool
  }
  /**
    * 连接池对象
    */
  private val jedisPool : JedisPool = createJedisPool()
}
