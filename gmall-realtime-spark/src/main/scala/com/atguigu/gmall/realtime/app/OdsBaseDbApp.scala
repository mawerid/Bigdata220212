package com.atguigu.gmall.realtime.app

import java.util

import com.alibaba.fastjson.{JSON, JSONObject}
import com.atguigu.gmall.realtime.utils.{MyKafkaUtils, MyOffsetUtils, MyRedisUtils}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.TopicPartition
import org.apache.spark.SparkConf
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{HasOffsetRanges, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import redis.clients.jedis.Jedis

/**
  * 业务数据消费分流任务
  * 1. 准备实时环境
  * 2. 从Redis中读取offset
  * 3. 从Kafka中消费数据
  * 4. 提取offset
  * 5. 处理数据
  *    5.1 转换结构
  *    5.2 分流
  * 6. 刷写kafka缓冲区
  * 7. 提交offset
  */
object OdsBaseDbApp {
  def main(args: Array[String]): Unit = {
    //1. 准备实时环境
    val sparkConf: SparkConf = new SparkConf().setAppName("ods_base_db_app").setMaster("local[4]")
    val ssc: StreamingContext = new StreamingContext(sparkConf, Seconds(5))

    //2. 从Redis中读取offset
    val topic : String  = "ODS_BASE_DB_0212"
    val grouoId: String = "ODS_BASE_DB_GROUP"
    val offsets: Map[TopicPartition, Long] = MyOffsetUtils.readOffset(topic , grouoId)

    //3.从Kafka中消费数据
    var kafkaDStream: InputDStream[ConsumerRecord[String, String]] = null
    if(offsets.size > 0 ){
      kafkaDStream = MyKafkaUtils.getKakfaDStream(ssc, topic, grouoId ,offsets)
    }else{
      kafkaDStream = MyKafkaUtils.getKakfaDStream(ssc, topic, grouoId )
    }

    //4. 提取offset
    var offsetRanges: Array[OffsetRange] = null
    val offsetDStream: DStream[ConsumerRecord[String, String]] = kafkaDStream.transform(
      rdd => {
        offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
        rdd
      }
    )
    //5.处理数据
    //5.1 转换结构
    val jsonObjDStream: DStream[JSONObject] = offsetDStream.map(
      consumerRecord => {
        val msg: String = consumerRecord.value()
        val jsonObj: JSONObject = JSON.parseObject(msg)
        jsonObj
      }
    )
    //jsonObjDStream.print(100)
    //5.2 分流
    // 分流原则:
    // 事实表数据:   order_info => DWD_ORDER_INFO_0212_I
    //                         => DWD_ORDER_INFO_0212_U
    //                         => DWD_ORDER_INFO_0212_D
    // 维度表数据:  分流到Redis中



    //事实表清单
    //val factTables: Set[String] = Set[String]("order_info" , "order_detail" /*需要什么表写什么表*/)
    //维度表清单
    //val dimTables: Set[String] = Set[String]("user_info" , "base_province" /*需要什么表写什么表*/)

    //
    jsonObjDStream.foreachRDD(
      rdd => {
         // Driver
         //TODO 如何动态维护表清单?
         // 将表清单维护到redis中,代码中只要保证能够周期性的读取redis中维护的表清单.
         // Redis中如何存储:
         // type:     set
         // key :     FACT:TABLES   DIM:TABLES
         // value:   表名的集合
         // 写入API:  sadd
         // 读取API:  smembers
         // 是否过期: 不过期
         val jedis: Jedis = MyRedisUtils.get()
         val factKey : String = "FACT:TABLES"
         val dimKey : String = "DIM:TABLES"
         val factTables: util.Set[String] = jedis.smembers(factKey)
         //做成广播变量
         val factTablesBC: Broadcast[util.Set[String]] = ssc.sparkContext.broadcast(factTables)
         println("factTables: " + factTables)
         val dimTables: util.Set[String] = jedis.smembers(dimKey)
         //做成广播变量
         val dimTablesBC: Broadcast[util.Set[String]] = ssc.sparkContext.broadcast(dimTables)
         println("dimTables: " + dimTables)

         MyRedisUtils.close(jedis)
          //
          rdd.foreachPartition(
            jsonObjIter => {
              val jedis: Jedis = MyRedisUtils.get()
              for (jsonObj <- jsonObjIter) {
                //获取操作类型
                val operType: String = jsonObj.getString("type")
                //判断类型:
                // 1. 过滤感兴趣的操作
                // 2. 明确当前数据的操作类型
                val op: String = operType match {
                  case "bootstrap-insert" => "I"
                  case "insert" => "I"
                  case "update" => "U"
                  case "delete" => "D"
                  case _ => null
                }
                if(op != null ){
                  //提取表名
                  val tableName: String = jsonObj.getString("table")
                  //提取data
                  val dataObj: JSONObject = jsonObj.getJSONObject("data")
                  //判断是事实表还是维度表
                  if(factTablesBC.value.contains(tableName)){
                    //事实表数据
                    //发往对应的主题
                    val topicName : String = s"DWD_${tableName.toUpperCase()}_0212_$op"
                    MyKafkaUtils.send(topicName , dataObj.toJSONString)
                    //if(tableName.equals("order_detail")){
                    //  Thread.sleep(300)
                    //}
                  }
                  if(dimTablesBC.value.contains(tableName)){
                    //TODO 历史维度数据如果不发生修改， 怎么保证进入到dim层(redis中)
                    // 在实时任务启动时， 进行一次全量同步.

                    //维度表数据
                    //Redis中如何存储
                    //type:  string
                    //key :  DIM:[tableName]:[id]
                    //value: json
                    //写入API: set
                    //读取API: get
                    //是否过期: 不过期

                    //从dataObj中获取id
                    val dataId: String = dataObj.getString("id")
                    //TODO 此处获取Redis连接好不好?
                    //不好， 每条数据都要获取一次连接， 用完直接还回去.
                    //我们希望获取一次连接，反复使用，最终还回去.
                    //val jedis: Jedis = MyRedisUtils.get()
                    val redisKey : String = s"DIM:${tableName.toUpperCase()}:$dataId"
                    jedis.set(redisKey , dataObj.toJSONString)

                    //MyRedisUtils.close(jedis)
                  }
                }
              }
              MyRedisUtils.close(jedis)
              //刷写Kafka缓冲区
              MyKafkaUtils.flush()
            }
          )
      // 提交Offset
        MyOffsetUtils.saveOffset(topic, grouoId , offsetRanges)
      }
    )

    ssc.start()
    ssc.awaitTermination()
  }
}
