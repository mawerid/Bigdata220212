package com.atguigu.gmall.realtime.app

import java.lang

import com.alibaba.fastjson.serializer.SerializeConfig
import com.alibaba.fastjson.{JSON, JSONArray, JSONObject}
import com.atguigu.gmall.realtime.bean.{PageActionLog, PageDisplayLog, PageLog, StartLog}
import com.atguigu.gmall.realtime.utils.{MyKafkaUtils, MyOffsetUtils}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.TopicPartition
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{HasOffsetRanges, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * 日志数据消费分流任务
  * 1. 准备实时环境
  * 2. 从Redis中读取offset
  * 3. 从Kafka中消费数据
  * 4. 提取offset
  * 5. 处理数据
  *    5.1 转换结构
  *    5.2 分流
  * 6. 刷写kafka缓冲区
  * 7. 提交offset
  */
object OdsBaseLogApp {
  def main(args: Array[String]): Unit = {

    //1. 准备实时环境
    val sparkConf: SparkConf = new SparkConf().setAppName("ods_base_log_app").setMaster("local[4]")
    val ssc: StreamingContext = new StreamingContext(sparkConf , Seconds(5))

    //2.从Kafka中消费数据
    val topic : String = "ODS_BASE_LOG_0212"
    val groupId : String = "ODS_BASE_LOG_GROUP"

    //TODO 补充: 从Redis中读取offset
    val offsets: Map[TopicPartition, Long] = MyOffsetUtils.readOffset(topic, groupId)
    var kafkaDStream: InputDStream[ConsumerRecord[String, String]] = null
    if(offsets.size > 0 ){
      //基于指定的offset进行消费（咱们说了算）
      kafkaDStream = MyKafkaUtils.getKakfaDStream(ssc,topic,groupId,offsets)
    }else{
      //基于默认的offset进行消费（kafka说了算）
      kafkaDStream = MyKafkaUtils.getKakfaDStream(ssc, topic , groupId)
    }

    //TODO 补充: 从流中提取offset信息
    var offsetRanges: Array[OffsetRange] = null
    val offsetDStream: DStream[ConsumerRecord[String, String]] = kafkaDStream.transform(
      rdd => {
        // 提取offset
        offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
        rdd
      }
    )

    //3. 处理数据
    // 3.1 转换结构
    //        通用结构   Map  List  Json
    //        专用结构   Bean
    val jsonObjDStream: DStream[JSONObject] = offsetDStream.map(
      consumerRecord => {
        val msg: String = consumerRecord.value()
        //将每条json消息转换成Json对象
        val jsonObj: JSONObject = JSON.parseObject(msg)
        jsonObj
      }
    )
    //jsonObjDStream.print(100)
    //3.2 分流
    //分流原则:
    //  错误数据: 只要数据中包含"err"字段，就定义为错误数据，直接发往错误主题 => DWD_ERROR_TOPIC_0212
    //  页面日志数据:
    //        页面访问数据:  common+page + ts    => DWD_PAGE_TOPIC_0212
    //        曝光数据:      display + page + common + ts  => DWD_DISPLAY_TOPIC_0212
    //        事件数据:      action + page + common + ts  => DWD_ACTION_TOPIC_0212
    //  启动日志数据:  common + start + ts   => DWD_START_TOPIC_0212

    val dwd_error_topic : String = "DWD_ERROR_TOPIC_0212"
    val dwd_page_topic : String = "DWD_PAGE_TOPIC_0212"
    val dwd_display_topic : String ="DWD_DISPLAY_TOPIC_0212"
    val dwd_action_topic : String = "DWD_ACTION_TOPIC_0212"
    val dwd_start_topic : String = "DWD_START_TOPIC_0212"



    jsonObjDStream.foreachRDD(
      rdd => {

        rdd.foreachPartition(
            // 一个分区的数据
            jsonObjIter => {

              //处理每个分区中的每条数据
              for (jsonObj <- jsonObjIter) {
                //处理每条数据
                //1).分流错误数据
                val errObj: JSONObject = jsonObj.getJSONObject("err")
                if(errObj != null){
                  //发往错误主题
                  MyKafkaUtils.send(dwd_error_topic , jsonObj.toJSONString )
                }else{
                  //解析common数据
                  val commonObj: JSONObject = jsonObj.getJSONObject("common")
                  val ar: String = commonObj.getString("ar")
                  val uid: String = commonObj.getString("uid")
                  val os: String = commonObj.getString("os")
                  val ch: String = commonObj.getString("ch")
                  val isNew: String = commonObj.getString("is_new")
                  val md: String = commonObj.getString("md")
                  val mid: String = commonObj.getString("mid")
                  val vc: String = commonObj.getString("vc")
                  val ba: String = commonObj.getString("ba")

                  //解析ts
                  val ts: lang.Long = jsonObj.getLong("ts")

                  //2). 分流页面日志数据
                  val pageObj: JSONObject = jsonObj.getJSONObject("page")
                  if(pageObj != null ){
                    val pageId: String = pageObj.getString("page_id")
                    val lastPageId: String = pageObj.getString("last_page_id")
                    val pageItem: String = pageObj.getString("item")
                    val pageItemType: String = pageObj.getString("item_type")
                    val duringTime: lang.Long = pageObj.getLong("during_time")
                    val sourceType: String = pageObj.getString("source_type")

                    //将相关字段封装到PageLog
                    val pageLog: PageLog =
                      PageLog(mid,uid,ar,ch,isNew,md,os,vc,ba,pageId,lastPageId,pageItem,pageItemType,duringTime,sourceType,ts)
                    //发送到页面访问主题
                    MyKafkaUtils.send(dwd_page_topic , JSON.toJSONString(pageLog,new SerializeConfig(true)))

                    //分流曝光数据
                    val displayArrObj: JSONArray = jsonObj.getJSONArray("displays")
                    if(displayArrObj != null && displayArrObj.size() > 0 ){
                      for(i <- 0 until displayArrObj.size()){
                        val displayObj: JSONObject = displayArrObj.getJSONObject(i)
                        val displayType: String = displayObj.getString("display_type")
                        val displayItem: String = displayObj.getString("item")
                        val displayItemType: String = displayObj.getString("item_type")
                        val posId: String = displayObj.getString("pos_id")
                        val order: String = displayObj.getString("order")

                        //将相关字段封装到PageDisplayLog中
                        val pageDisplayLog: PageDisplayLog =
                          PageDisplayLog(mid,uid,ar,ch,isNew,md,os,vc,ba,pageId,lastPageId,pageItem,pageItemType,duringTime,sourceType,displayType,displayItem,displayItemType,order,posId,ts)
                        //发送到曝光主题
                        MyKafkaUtils.send(dwd_display_topic ,  JSON.toJSONString(pageDisplayLog , new SerializeConfig(true)))
                      }
                    }

                    //分流事件数据
                    val actionArrObj: JSONArray = jsonObj.getJSONArray("actions")
                    if(actionArrObj != null && actionArrObj.size() >0 ){
                      for(i <- 0 until actionArrObj.size()){
                        val actionObj: JSONObject = actionArrObj.getJSONObject(i)
                        val actionId: String = actionObj.getString("action_id")
                        val actionItem: String = actionObj.getString("item")
                        val actionItemType: String = actionObj.getString("item_type")
                        val actionTs: lang.Long = actionObj.getLong("ts")

                        //将相关字段封装到PageActionLog
                        val pageActionLog: PageActionLog =
                          PageActionLog(mid,uid,ar,ch,isNew,md,os,vc,ba,pageId,lastPageId,pageItem,pageItemType,duringTime,sourceType,actionId,actionItem,actionItemType,actionTs,ts)

                        //发送到事件主题
                        MyKafkaUtils.send(dwd_action_topic , JSON.toJSONString(pageActionLog , new SerializeConfig(true)) )
                      }
                    }
                  }

                  //3). 分流启动日志数据
                  val startObj: JSONObject = jsonObj.getJSONObject("start")
                  if(startObj != null){
                    val entry: String = startObj.getString("entry")
                    val openAdId: String = startObj.getString("open_ad_id")
                    val openAdMs: lang.Long = startObj.getLong("open_ad_ms")
                    val openAdSkipMs: lang.Long = startObj.getLong("open_ad_skip_ms")
                    val loadingTime: lang.Long = startObj.getLong("loading_time")

                    //将相关字段封装到StartLog中
                    val startLog: StartLog =
                      StartLog(mid,uid,ar,ch,isNew,md,os,vc,ba,entry,openAdId,loadingTime,openAdMs,openAdSkipMs,ts)

                    //发送到启动主题
                    MyKafkaUtils.send(dwd_start_topic , JSON.toJSONString(startLog , new SerializeConfig(true)))

                  }
                }
              }
              MyKafkaUtils.flush()
              //D： 算子(foreachPartition)里面:   Executor端执行，每批次每分区执行一次
            }
        )

        /*
        rdd.foreach(
          jsonObj => {

          //C : 算子(foreach)里面 : Executor端执行, 每批次每条数据执行一次.
          }
        )
         */

      //B: foreachRDD里面， 算子(foreach)外面:  Driver端执行. 每批次执行一次.
        MyOffsetUtils.saveOffset(topic, groupId , offsetRanges)
      }
    )

    //A: foreachRDD外面: Driver端执行. 程序启动的时候执行一次。

    ssc.start()
    ssc.awaitTermination()
  }
}
