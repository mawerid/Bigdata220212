package com.atguigu.gmall.realtime.utils

import java.util

import org.apache.kafka.common.TopicPartition
import org.apache.spark.streaming.kafka010.OffsetRange
import redis.clients.jedis.Jedis

import scala.collection.mutable

/**
  * offset管理: 将offset信息写入到redis中 及 从redis中读取offset信息
  */
object MyOffsetUtils {

  /**
    * 将offset信息存储到redis中
    *
    * offset结构:  (GTP)group + topic + partition => offset
    *                   g : t      0 => 100
    *                   g : t      1 => 101
    *                          =>
    *                   g : t      2 => 99
    *                   g : t      3 => 100
    * Redis中的存储:
    * type:    hash
    * key:     offset:[group]:[topic]
    * value:   [partition => offset] ...
    * 写入API:  hset
    * 读取API:  hgetall
    * 是否过期: 不过期
    */
  def saveOffset(topic : String,  groupId : String , offsetRanges: Array[OffsetRange]): Unit ={

    if(offsetRanges != null && offsetRanges.size > 0 ){
      //声明Map封装partition和offset
      val offsets: util.HashMap[String, String] = new util.HashMap[String,String]()
      for (offsetRange <- offsetRanges) {
        //分区
        val partition: Int = offsetRange.partition
        //offset
        val offset: Long = offsetRange.untilOffset
        offsets.put(partition.toString,offset.toString)
      }
      //获取redis连接
      val jedis: Jedis = MyRedisUtils.get()
      val redisKey : String = s"offset:$groupId:$topic"
      jedis.hset(redisKey , offsets)
      MyRedisUtils.close(jedis)
      println("保存offset: " + offsets)
    }
  }

  /**
    * 从redis中读取offset信息
    * Map[TopicPartition,Long] :
    *     TopicPartition : 封装topic 和 partition
    *     Long:  就是offset
    */
  def readOffset(topic : String, groupId : String ): Map[TopicPartition,Long] ={
    val results: mutable.Map[TopicPartition, Long] = mutable.Map[TopicPartition,Long]()
    val redisKey: String = s"offset:$groupId:$topic"
    val jedis: Jedis = MyRedisUtils.get()
    //从redis中读取对应的offset信息
    val offsets: util.Map[String, String] = jedis.hgetAll(redisKey)
    println("读到offset: " + offsets)
    if(offsets!= null && offsets.size() > 0 ){
      //将java集合转换成scala集合
      import scala.collection.JavaConverters._
      for ( (partition, offset) <- offsets.asScala) {
        //封装TopicPartition
        val tp: TopicPartition = new TopicPartition(topic, partition.toInt)
        //保存到结果中
        results.put(tp , offset.toLong)
      }
    }
    MyRedisUtils.close(jedis)
    results.toMap
  }

}
