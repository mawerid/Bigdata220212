package com.atguigu.phoenix;

import java.sql.*;

/**
 * Thin Client
 *
 * JDBC: （注册驱动） 获取连接  编写SQL  预编译SQL  设置参数  执行SQL  处理结果  关闭连接
 */
public class ThinClientDemo {

    public static void main(String[] args) throws SQLException {
        testSelect();
    }

    public static void testSelect() throws SQLException {
        String url = "jdbc:phoenix:thin:url=http://hadoop102:8765;serialization=PROTOBUF;" ;
        Connection connection = DriverManager.getConnection(url);
        String sql = "select id ,name ,addr from emp where id = ? ";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1 , "1001");
        ResultSet resultSet = ps.executeQuery();
        if(resultSet.next()){
            String id  = resultSet.getString("id");
            String name = resultSet.getString("name");
            String addr = resultSet.getString("addr");
            System.out.println(id + " : " + name + " : " + addr );
        }
        resultSet.close();
        ps.close();
        connection.close();
    }

    /**
     * Phoenix默认是不自动提交。
     *
     * connection.setAutoCommit();
     * connection.commit();
     */
    public static void testDelete(){}

    public static void testUpsert(){}

}
