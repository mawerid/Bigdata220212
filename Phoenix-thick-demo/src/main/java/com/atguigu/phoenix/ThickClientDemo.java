package com.atguigu.phoenix;

import java.sql.*;
import java.util.Properties;

/**
 * Thick Client
 * JDBC: （注册驱动） 获取连接  编写SQL  预编译SQL  设置参数  执行SQL  处理结果  关闭连接
 */
public class ThickClientDemo {
    public static void main(String[] args) throws SQLException {
        testSelect();
    }

    public static void testSelect() throws SQLException {

        String url = "jdbc:phoenix:hadoop102,hadoop103,hadoop104:2181" ;
        Properties props = new Properties();
        props.put("phoenix.schema.isNamespaceMappingEnabled" ,"true");
        Connection connection = DriverManager.getConnection(url,props);
        String sql = "select id ,name ,addr from emp where id = ? ";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1 , "1001");
        ResultSet resultSet = ps.executeQuery();
        if(resultSet.next()){
            String id  = resultSet.getString("id");
            String name = resultSet.getString("name");
            String addr = resultSet.getString("addr");
            System.out.println(id + " : " + name + " : " + addr );
        }
        resultSet.close();
        ps.close();
        connection.close();

    }
}
