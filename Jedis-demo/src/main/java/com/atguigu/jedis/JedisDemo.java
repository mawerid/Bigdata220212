package com.atguigu.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * 测试Jedis使用
 *
 * 需要注意的问题：
 *   1. linux的防火墙需要关掉
 *   2. redis配置文件中的 bind 和 protected-mode
 */
public class JedisDemo {

    public static void main(String[] args) {

        //Jedis jedis = getJedis();
/*        Jedis jedis = getJedisFromPool();

        String ping = jedis.ping();
        System.out.println(ping);

        close(jedis);*/

        testString();
    }

    /**
     * 测试每种类型对应的API方法
     */
    public static void testString(){
        Jedis jedis = getJedisFromPool();
        // 命令: set get  append setnx incr ......
        // API:
        jedis.set("username", "admin");
        String value = jedis.get("username");
        System.out.println(value);

        close(jedis);
    }

    public static void testList(){}

    public static void testSet(){}

    public static void testZset(){}

    public static void testHash(){}



    /**
     * 基于连接池技术维护Jedis连接对象
     */
    public static JedisPool jedisPool = null  ;

    public static Jedis getJedisFromPool(){

        if(jedisPool == null ){
            //主要配置
            JedisPoolConfig jedisPoolConfig =new JedisPoolConfig();
            jedisPoolConfig.setMaxTotal(10); //最大可用连接数
            jedisPoolConfig.setMaxIdle(5); //最大闲置连接数
            jedisPoolConfig.setMinIdle(5); //最小闲置连接数
            jedisPoolConfig.setBlockWhenExhausted(true); //连接耗尽是否等待
            jedisPoolConfig.setMaxWaitMillis(2000); //等待时间
            jedisPoolConfig.setTestOnBorrow(true); //取连接的时候进行一下测试 ping pong

            String host = "hadoop102" ;
            int port = 6379 ;
            jedisPool = new JedisPool( jedisPoolConfig , host, port  );
        }

        Jedis jedis = jedisPool.getResource();
        return jedis ;

    }


    /**
      创建Jedis对象
     */
    public static Jedis getJedis(){
        String host = "hadoop102" ;
        int port = 6379 ;
        Jedis jedis = new Jedis(host, port );
        return jedis ;
    }

    /*
      关闭Jedis对象
     */
    public static void close(Jedis jedis){
        if(jedis != null) jedis.close();
    }

}
