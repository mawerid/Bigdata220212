package com.atguigu.es;

import com.alibaba.fastjson.JSON;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.ParsedAvg;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ES 客户端
 */
public class EsClientDemo {

    public static void main(String[] args) throws IOException {
        //System.out.println(esClient);
        //testSave();
        //bulkSave();
        //deleteById();
        //deleteByQuery();
        //updateById();
        //updateByQuery();
        //searchById();
        //searchByCondition();
        searchByAggs();

        esClient.close();
    }

    /**
     * 查询 - 聚合
     *
     * 查询每位演员参演的电影的平均分，倒叙排序
     */
    public static void searchByAggs() throws IOException {
        String indexName = "movie_index" ;
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //group aggs
        TermsAggregationBuilder termsAggregationBuilder =
                AggregationBuilders.
                        terms("groupbyactorname").
                        field("actorList.name.keyword").
                        size(10).
                        order(BucketOrder.aggregation("avgscore" , false));
        //avg aggs
        AvgAggregationBuilder avgAggregationBuilder = AggregationBuilders.avg("avgscore").field("doubanScore");
        termsAggregationBuilder.subAggregation(avgAggregationBuilder);
        searchSourceBuilder.aggregation(termsAggregationBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = esClient.search(searchRequest, RequestOptions.DEFAULT);
        Aggregations aggregations = searchResponse.getAggregations();
        ParsedTerms parsedTerms = aggregations.get("groupbyactorname");
        List<? extends Terms.Bucket> buckets = parsedTerms.getBuckets();
        for (Terms.Bucket bucket : buckets) {
            String actorName = bucket.getKeyAsString();
            long movieCount = bucket.getDocCount();
            Aggregations bucketAggregations = bucket.getAggregations();
            ParsedAvg parsedAvg = bucketAggregations.get("avgscore");
            double avgScore = parsedAvg.getValue();
            System.out.println(actorName  + " 共参演了 " + movieCount + " 部电影， 平均分 " + avgScore + " 分！");
        }
    }


    /**
     * 查询 - 过滤
     *     查询doubanScore>=5.0     filter
     *     关键词搜索red sea     match
     *     关键词高亮显示     highlight
     *     显示第一页，每页2条    from + size
     *     按doubanScore从大到小排序    sort
     */

    public static void searchByCondition() throws IOException {
        String indexName = "movie_index" ;
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //bool
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //match
        MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("name","red sea");
        //must
        boolQueryBuilder.must(matchQueryBuilder);
        //range
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("doubanScore").gte(5.0);
        //filter
        boolQueryBuilder.filter(rangeQueryBuilder);
        //query
        searchSourceBuilder.query(boolQueryBuilder);

        //higtlight
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("name");
        searchSourceBuilder.highlighter(highlightBuilder);

        //分页:  当前页pageNum   每页条数PageSize
        int pageNum = 1 ;
        int pageSize = 2 ;
        int from  = (pageNum -1) * pageSize ;
        //from
        searchSourceBuilder.from( from );
        //size
        searchSourceBuilder.size(pageSize);

        //sort
        searchSourceBuilder.sort("doubanScore" , SortOrder.DESC);

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = esClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHit[] searchHits = searchResponse.getHits().getHits();
        for (SearchHit searchHit : searchHits) {
            //获取明细数据
            Map<String, Object> sourceAsMap = searchHit.getSourceAsMap();
            System.out.println("高亮前result : " + sourceAsMap);
            //处理高亮
            Map<String, HighlightField> highlightFields = searchHit.getHighlightFields();
            HighlightField highlightField = highlightFields.get("name");
            Text[] fragments = highlightField.getFragments();
            String highlightvalue = fragments[0].toString();
            //将高亮的内容替换到明细数据中
            sourceAsMap.put("name" , highlightvalue);
            System.out.println("高亮后result : " + sourceAsMap);
            System.out.println("------------------------------------");
        }
    }


    /**
     * 查询 - 基于id
     */
    public static void searchById() throws IOException {
        String indexName = "movietest";
        String docId ="1001";
        GetRequest getRequest = new GetRequest(indexName , docId);
        GetResponse getResponse = esClient.get(getRequest, RequestOptions.DEFAULT);
        Map<String, Object> sourceMap = getResponse.getSource();
        System.out.println("sourceMap : " + sourceMap);

        String sourceString = getResponse.getSourceAsString();
        System.out.println("sourceString : " + sourceString);

    }


    /**
     * 修改 - 基于query
     */
    public static void updateByQuery() throws IOException {
        String indexName = "movietest";
        UpdateByQueryRequest updateByQueryRequest = new UpdateByQueryRequest(indexName);
        //term
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("movieName.keyword", "战狼2");
        //query
        updateByQueryRequest.setQuery(termQueryBuilder);

        //script
        HashMap<String, Object> params = new HashMap<>();
        params.put("newName" , "战狼3");
        Script script = new Script(
                ScriptType.INLINE,
                Script.DEFAULT_SCRIPT_LANG ,
                "ctx._source['movieName']=params.newName",
                params);
        updateByQueryRequest.setScript(script);

        esClient.updateByQuery(updateByQueryRequest , RequestOptions.DEFAULT);
    }


    /**
     * 修改 - 基于docId
     */

    public static void updateById() throws IOException {
        String indexName = "movietest" ;
        String docId = "1001" ;
        UpdateRequest updateRequest = new UpdateRequest(indexName,docId);
        updateRequest.doc("movieName" , "红海行动");
        esClient.update(updateRequest , RequestOptions.DEFAULT);
    }


    /**
     * 删除 - 基于query
     */

    public static void deleteByQuery() throws IOException {
        String indexName = "movietest";
        DeleteByQueryRequest deleteByQueryRequest = new DeleteByQueryRequest(indexName);

        //bool
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //term
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("movieName.keyword" , "长津湖");
        //filter
        boolQueryBuilder.filter(termQueryBuilder);
        //query
        deleteByQueryRequest.setQuery(boolQueryBuilder);

        esClient.deleteByQuery(deleteByQueryRequest , RequestOptions.DEFAULT);
    }

    /**
     * 删除 - 基于docId
     */

    public static void deleteById() throws IOException {
        String indexName = "movietest";
        String docId = "1Ld1qIEB1WnfOeIw4SK8";
        DeleteRequest deleteRequest = new DeleteRequest(indexName,docId);
        //deleteRequest.id(docId);
        esClient.delete(deleteRequest , RequestOptions.DEFAULT);
    }



    /**
     * 批量写入
     */
    public static void bulkSave() throws IOException {
        String indexName = "movietest";
        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(new Movie("1002" , "战狼"));
        movies.add(new Movie("1003" , "速度与激情"));
        movies.add(new Movie("1004" , "长津湖"));

        BulkRequest bulkRequest = new BulkRequest(indexName); // 设置全局索引

        //将每条数据的写入封装成一个IndexRequest对象.
        for (Movie movie : movies) {
            IndexRequest indexRequest = new IndexRequest();
            indexRequest.id(movie.getId());
            indexRequest.source(JSON.toJSONString(movie) , XContentType.JSON);

            bulkRequest.add(indexRequest);
        }

        esClient.bulk(bulkRequest , RequestOptions.DEFAULT);

    }




    /**
     * 单条数据写入
     *    幂等写入:  指定docid
     *    非幂等写入: 不指定docid
     */

    public static void testSave() throws IOException {
        String indexName = "movietest";
        Movie movie = new Movie("1001", "红海行动");

        IndexRequest indexRequest = new IndexRequest(indexName); //指定索引
        //indexRequest.index(indexName); //指定索引
        // 如果是幂等写，要指定docid
        //indexRequest.id(movie.getId()) ;
        indexRequest.source(JSON.toJSONString(movie) , XContentType.JSON); //指定数据

        esClient.index(indexRequest , RequestOptions.DEFAULT);
    }



    public static RestHighLevelClient esClient = create() ;

    public static RestHighLevelClient create(){
        HttpHost httpHost = new HttpHost("hadoop102", 9200);
        RestClientBuilder builder = RestClient.builder(httpHost);
        RestHighLevelClient client = new RestHighLevelClient(builder);
        return client ;
    }

}

class Movie{
    private String id ;
    private String movieName ;

    public Movie(String id, String movieName) {
        this.id = id;
        this.movieName = movieName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }
}