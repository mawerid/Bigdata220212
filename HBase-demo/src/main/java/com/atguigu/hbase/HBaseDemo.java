package com.atguigu.hbase;  

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * HBase API
 *
 * Connection:
 *    Admin
 *    Table
 */
public class HBaseDemo {

    public static void main(String[] args) throws IOException {
        //System.out.println(connection);
        //createTable(null , "user" , "info1", "info2");
        //putData(null , "user" , "1001" , "info1" , "name" , "zhangxiaosan");
        //deleteData(null , "stu" , "1003" , "info1" ,"name" );
        //getData(null ,"stu" , "1004");
        scanData(null , "stu" , "1002" , "1002|");

    }

    /**
     * DML  - Table - scan
     */
    public static void scanData(String namespace , String tableName , String startRow , String stopRow ) throws IOException {
        TableName tn = TableName.valueOf(namespace, tableName);
        Table table = connection.getTable(tn);

        Scan scan = new Scan();
        scan.withStartRow(Bytes.toBytes(startRow)).withStopRow(Bytes.toBytes(stopRow));
        ResultScanner resultScanner = table.getScanner(scan);
        Iterator<Result> iterator = resultScanner.iterator();
        while(iterator.hasNext()){
            Result next = iterator.next(); // 一行数据
            List<Cell> cells = next.listCells();
            for (Cell cell : cells) {
                String column = Bytes.toString( CellUtil.cloneRow(cell))  + " : " +
                        Bytes.toString( CellUtil.cloneFamily( cell )) + " : " +
                        Bytes.toString( CellUtil.cloneQualifier(cell )) + " : " +
                        Bytes.toString(CellUtil.cloneValue(cell)) ;
                System.out.println(column);
            }

            System.out.println("-----------------------------------");
        }

        table.close();

    }

    /**
     * DML  - Table - get
     */
    public static void getData(String namespace , String tableName , String rk ) throws IOException {
        TableName tn = TableName.valueOf(namespace, tableName);
        Table table = connection.getTable(tn);

        Get get = new Get(Bytes.toBytes(rk));
        //get.addColumn()  指定具体的列
        //get.addFamily()  指定列族
        Result result = table.get(get);
        List<Cell> cells = result.listCells();
        for (Cell cell : cells) {
            String column  =
                     Bytes.toString( CellUtil.cloneRow(cell) ) + " : " +
                     Bytes.toString(CellUtil.cloneFamily(cell) ) + " : " +
                     Bytes.toString(CellUtil.cloneQualifier(cell)) + " : " +
                     Bytes.toString(CellUtil.cloneValue(cell)) ;
            System.out.println(column);
        }
        table.close();
    }


    /**
     * DML  - Table - delete
     */
    public static void deleteData(String namespace, String tableName, String rk, String cf , String cl ) throws IOException {
        TableName tn = TableName.valueOf(namespace, tableName);
        Table table = connection.getTable(tn);

        //DeleteFamily
        //Delete delete = new Delete(Bytes.toBytes(rk));

        //DeleteFamily
        //Delete delete = new Delete(Bytes.toBytes(rk));
        //delete.addFamily(Bytes.toBytes(cf));

        //DeleteColumn
        Delete delete = new Delete(Bytes.toBytes(rk));
        delete.addColumns(Bytes.toBytes(cf), Bytes.toBytes(cl));

        //Delete
        //Delete delete = new Delete(Bytes.toBytes(rk));
        //delete.addColumn(Bytes.toBytes(cf) , Bytes.toBytes(cl));

        table.delete(delete);

        table.close();
    }



    /**
     * DML  - Table - put(插入、修改)
     */
    public static void putData(String namespace, String tableName , String rk ,  String cf, String cl, String value ) throws IOException {
        //获取Table对象
        TableName tn = TableName.valueOf(namespace, tableName);
        Table table = connection.getTable(tn);

        Put put = new Put( Bytes.toBytes(rk));
        put.addColumn(Bytes.toBytes(cf) , Bytes.toBytes(cl) , Bytes.toBytes(value));
        table.put(put);

        table.close();
    }


    /**
     * DDL:  namespace / Table
     *
     * 创建表
     *
     * 其他的ddl操作大家自行测试.
     */
    public static void createTable(String namespace , String tableName , String ... cfs) throws IOException {
        // 基本的判空处理

        Admin admin = connection.getAdmin();

        // 1. 判断表是否存在
        TableName tn = TableName.valueOf(namespace, tableName);
        boolean exists = admin.tableExists(tn);
        if(exists){
            System.err.println( (namespace ==null? "default" : namespace) + ":" + tableName + "已经存在!");
            return ;
        }
        // 2. 创建表
        TableDescriptorBuilder tableDescriptorBuilder = TableDescriptorBuilder.newBuilder(tn);

        //设置列族
        if(cfs.length <=0){
            System.err.println("至少指定一个列族!");
            return ;
        }
        for (String cf : cfs) {
            ColumnFamilyDescriptorBuilder columnFamilyDescriptorBuilder =
                        ColumnFamilyDescriptorBuilder.newBuilder(Bytes.toBytes(cf));
            ColumnFamilyDescriptor columnFamilyDescriptor = columnFamilyDescriptorBuilder.build();
            tableDescriptorBuilder.setColumnFamily(columnFamilyDescriptor);
        }

        TableDescriptor tableDescriptor = tableDescriptorBuilder.build();
        admin.createTable(tableDescriptor);

        System.out.println((namespace ==null? "default" : namespace) + ":" + tableName + "创建成功!");

        admin.close();
    }



    private static  Connection connection ;

    static{
        try {
            //Configuration conf = new Configuration();
            Configuration conf = HBaseConfiguration.create();
            //通过zk找到hbase
            conf.set("hbase.zookeeper.quorum","hadoop102,hadoop103,hadoop104");
            connection = ConnectionFactory.createConnection(conf);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
